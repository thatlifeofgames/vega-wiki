---
title: Contributing
description: Contribute some guides to this website for a reward!
published: true
date: 2021-08-29T16:57:25.389Z
tags: 
editor: markdown
dateCreated: 2021-07-13T15:01:48.234Z
---

# Contributing
You can contribute new guides to the wiki here for free VIP (and other rewards) in the Minecraft server. The amount of VIP you get depends on how much you have contributed.

You can see a reward for an incomplete page by checking the page. Contributions may be rejected if they are not up to standard, and a reward is not guaranteed.

Please note your contributions will be licenced by the [Attribution-NonCommercial 4.0 International ](https://creativecommons.org/licenses/by-nc/4.0/) licence. **You should understand this before making any contribution**. This is not legal advice.

<br />

## How do I contribute
You can submit a merge request to the [Gitlab repository](https://gitlab.com/thatlifeofgames/vega-wiki/). **Please make sure you understand everything on this page first.**

Please **contact Chris.#9462 on [Discord](https://vega.games/discord) before starting a contribution**, or if you want to create a new page/guide that is not currently listed. This will avoid conflicts and unwanted content. You will need a Gitlab account to submit your contributions as a merge request. 
<br />

## Contributing rewards

> As VIP is not yet ready, any VIP rewards will not be given straight away, and instead, be given in a few weeks once it is complete.
{.is-success}

Rewards are shown on the page, however, they are flexible, if you are not interested in the reward shown on the page and instead would be interested in something else, please contact me (Chris) first.

If a page states it does not have a reward, it is usually planned to be done by me (Chris).

<br />

## Small contributions

Small contributions (such as typos, spelling mistakes and minor updates) may not invoke a reward, however they are greatly appreciated. If you submit lots of small contributions, I may still give you a reward.

# Contribution guidelines
(unfinished)


- Contributions must be your **own** work.
- Must be written in good English, with good grammar, mistakes are fine but will need to be fixed before merging.
- Please mention your Minecraft username in the merge request description if you would like a reward (if applicable).
- 


<br />
