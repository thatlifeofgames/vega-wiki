---
title: Welcome
description: Welcome to Vega's website! You can find help and other resources here.
published: true
date: 2021-08-29T16:05:54.440Z
tags: 
editor: markdown
dateCreated: 2021-07-13T14:36:44.309Z
---

# What is this?

Vega is an advanced Minecraft Skyblock server with endless opportunities. We try to provide a great experience without any unnecessary distractions. You can play with friends or by yourself, maybe even find some new people to team up with.

Feel free to join the server at **Vega.games**.

## Wiki

> The wiki is currently a work in progress, you can help finish it by [contributing](/contributing)
{.is-info}

Vega is a very new server, and I prioritised working on the server itself rather than the wiki. So currently, the wiki is very unfinished. You can help out by checking the contributing page.

If you have ideas for new pages, please let me (Chris) know on the [discord](/discord).
