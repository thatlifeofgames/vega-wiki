---
title: Farming
description: Learn how to make yourself food and spawn animals.
published: true
date: 2021-07-14T15:23:14.866Z
tags: 
editor: markdown
dateCreated: 2021-07-14T11:47:39.017Z
---

> This page is unfinished, contributors finishing the page will be rewarded with 14 days free VIP.
{.is-info}

Notes for page
- How to spawn animals
- How to get different crops (link to other pages eg mob farming)
- How to build your farm efficiently
