---
title: Mob farming
description: Learn about the different mob farms you can build to collect more resources.
published: true
date: 2021-07-14T15:27:47.744Z
tags: 
editor: markdown
dateCreated: 2021-07-14T15:27:46.353Z
---

> This page is unfinished, contributors finishing the page will be rewarded with 14 days free VIP.
{.is-info}

Notes for page
- Needs to include images (you can do this in creative on a singleplayer world)
- Needs to include at least 2 of the different types, one is easier to build and the other is more efficient
- Has to include why you would want one (eg to collect rarer items like iron, and general mob drops for trading)